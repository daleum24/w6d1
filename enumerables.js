// multiples

Array.prototype.multiples = function(){
  var multiples = []
  for(var i = 0; i < this.length; i++){
    multiples.push(this[i] * 2);
  }
  return multiples
}

// console.log([1,2,3,4,5].multiples());

//my_each

Array.prototype.myEach = function(func){
  for(var i=0; i < this.length; i ++){
    //func.call(this[i]);
    func(this[i]);
  }
  return this
};

//var fun = function(el){console.log(el)};
//console.log([1,2,3,4].myEach(fun));

Array.prototype.myMap = function(func){
  var mapped = [];
  function fun(f){
    mapped.push(func(f))
  }
  this.myEach(fun)
  return mapped
};
//console.log([1,2,3,4].myMap(function(el){return el*2}));


Array.prototype.myInject = function(func){
  var arr = this
  var accumulator = arr.shift()
  function fun(e){
    //accumulator += e
    accumulator = func(accumulator,e)
  }
  arr.myEach(fun)
  return accumulator
};

// var inj = function(a,e){return a + e};
// console.log([2,2,3,4].myInject(inj));




