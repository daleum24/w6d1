// remove dups

Array.prototype.my_uniq = function(){
  var uniq = [];
  for(i = 0; i < this.length; i ++){
    if (uniq.indexOf(this[i]) == -1) {
      uniq.push(this[i]);
    }
  }
  return uniq
};


// console.log([1,2,1,3,3].my_uniq())
// console.log("hello")

// two sum

Array.prototype.two_sum = function(){
  var two_sum = [];
  for(var i=0; i < this.length; i++){
    for (var j=i+1; j<this.length; j++){
      if (this[j] + this[i] == 0) {
        two_sum.push([i,j])
      }
    }

  }
  return two_sum;
};

//console.log([-1, 0, 2, -2, 1].two_sum());


Array.prototype.my_transpose = function(){
  // var transposed = [ [1,1,1],[1,1,1],[1,1,1] ];

  var transposed = new Array(this.length);
  for (var i = 0; i < this.length; i++) {
    transposed[i] = new Array(this.length);
  }

  for(var i=0; i < this.length; i++){
    for (var j=0; j<this.length; j++){
      transposed[j][i] = this[i][j]
    }
  }
  return transposed;
}

// var rows = rows = [
//     [0, 1, 2],
//     [3, 4, 5],
//     [6, 7, 8]
//   ];
//
// console.log(rows.my_transpose());
