function Cat(name, owner){
  this.name = name,
  this.owner = owner
}

Cat.prototype.cuteStatement = function(){
  return this.owner + ' loves ' + this.name;
}

c = new Cat("Fred", "Dale");
console.log(c.cuteStatement());

Cat.prototype.cuteStatement = function(){
  return 'everyone loves ' + this.name;
}

console.log(c.cuteStatement());
d = new Cat("Sennacy", "Ryan");
console.log(d.cuteStatement());

Cat.prototype.meow = function(){
  return "meow, bro"
}

console.log(c.meow());
c.meow = function(){
  return "some other silly stuff"
};
console.log(c.meow());
console.log(d.meow());