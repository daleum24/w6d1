function range(start,end){
  var rng = []
  if (start == (end - 2)){
    rng.push(end-1);
    return rng;
  } else {
    rng.push(start + 1)
    return rng.concat(range(start + 1, end ))
    // rng.push(range(start, end - 1))
    // return rng
  }
};
//console.log(range(1,4))

//sum of an array
function itSumOfArray(arr) {
  var sum = 0
  for (var i=0; i<arr.length; i++) {
    sum += arr[i]
  }
  return sum
}

function recSumOfArray(arr) {
  if (arr.length == 1){
    return arr[0]
  } else {
    return arr[0] + recSumOfArray(arr.slice(1,arr.length))
  }
}

// console.log(recSumOfArray([1,2,3,4]))

//Exponentiation
function exp1(b, p){
  if (p == 0){
    return 1
  } else {
    return b * exp1(b, p-1);
  }
};

function exp2(b, p){
  if (p == 0){
    return 1;
  } else if (p % 2 == 0) {
    return Math.pow(exp2(b, p / 2) ,2);
  } else {
    return b * Math.pow(exp2(b, (p-1) / 2) ,2);
  }
};
// console.log(exp2(2,3));

//Fibonacci
function recFib(n){
  if (n == 1){
    return [0];
  } else if (n == 2) {
    return [0, 1];
  } else {
    var a = recFib(n-1)
    //console.log(a);
    a.push(a[a.length-1] + a[a.length-2]);
    return a;
  }
};

//console.log(recFib(7));

function itFib(n){
  var fibs = [];
  for(var i = 0; i < n; i++){
    if (i == 0 || i == 1){
      fibs.push(i)
    } else {
      fibs.push(fibs[fibs.length-1] + fibs[fibs.length-2])
    }
  }
  return fibs

};


//console.log(itFib(7));

function binarySearch(arr,target){
  if (arr.length <= 1){
    return (arr.indexOf(target) >= 0) ? arr.indexOf(target) : NaN
  } else if (arr[Math.floor(arr.length/2)] <= target) {
    console.log(arr);
    return Math.floor(arr.length/2) + binarySearch(arr.slice(Math.floor(arr.length/2),arr.length),target);
  } else {
    console.log(arr);
    return binarySearch(arr.slice(0,Math.floor(arr.length/2)),target);
  }
}

//console.log(binarySearch([1,2,3,4,5,6,7,8],7))

function makeChange(n, coins){
  coins = coins || [25,10,5,1]
  change_set = []
  for(var i = 0; i < coins.length; i++){
    change_set.push(makeChangeOnce(n, coins.slice(i,coins.length)));
  }

  change_set.sort(function(a,b){ return a.length - b.length });
  return change_set[0]
}


function makeChangeOnce(n, coins){
  coins = coins || [25,10,5,1];
  change = []

  if (coins.length == 1) {
    for(var i = 0; i < n; i++) {
      change.push(1)
    }
    return change
  } else {
    coinCount = Math.floor(n/coins[0])
    for (var i=0; i < coinCount; i++) {
      change.push(coins[0])
      n -= coins[0]
    }
    return change.concat(makeChangeOnce(n,coins.slice(1,coins.length)))
  }

};

//console.log(makeChange(14,[10,7,1]))

//merge_sort

function merge_sort(arr){
  if (arr.length == 2){
    return merge([arr[0]],[arr[1]]);
  } else {
    return merge(merge_sort(arr.slice(0,arr.length/2)),merge_sort(arr.slice(arr.length/2,arr.length)))
    //return merge_sort()
  }

}

function merge(half1, half2){
  var merged = []
  while (half1.length > 0 && half2.length > 0){
    if (half1[0] > half2[0]){
      merged.push(half2.shift())
    } else {
      merged.push(half1.shift())
    }
  }
  merged = merged.concat(half1).concat(half2)
  return merged
};

//console.log(merge([5],[6]));
// console.log(merge_sort([6,5,4,3,1,2,7,8]));

function subsets(arr){
  if (arr.length == 0){
    return [[]];
  } else {
    var previous = subsets(arr.slice(0,arr.length-1))
    var new_subs = []
    for (var i=0; i< previous.length; i++){
      new_subs.push(previous[i].concat([arr[arr.length-1]]))
    }
    return previous.concat(new_subs);
  }
}

console.log(subsets([1]))






























