// bubble sort
Array.prototype.clone = function() { return this.slice(0); }

Array.prototype.bubbleSort = function(){
  var sorted = this
  var is_sorted = false
  while (!is_sorted){
    is_sorted = true
    //sorted might change
    for (var i=0; i<this.length-1; i++){
      if (sorted[i] > sorted[i+1]){
        is_sorted = false
        var temp = sorted[i];
        sorted[i] = sorted[i+1];
        sorted[i+1] = temp;
      }
    }
  }
  return sorted
};

// console.log([1,2,3].clone())
// console.log([5,3,2,1,4].bubbleSort())

//substrings
function substrings(str){
  var subs = []
  for( var i = 0; i < str.length; i ++){
    for( var j = i; j < str.length; j ++){
      subs.push(str.substring(i,j+1));
    }
  }
  return subs
}

// console.log(substrings("cat"));



