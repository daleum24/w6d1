Array.prototype.my_uniq = function(){
  var uniq = [];
  for(i = 0; i < this.length; i ++){
    if (uniq.indexOf(this[i]) == -1) {
      uniq.push(this[i]);
    }
  }
  return uniq
};

Array.prototype.compare = function(arr){
  if (arr.length != this.length){
    return false;
  } else {
    var flag = true
    for (var i=0; i<arr.length; i++){
      if (arr[i] != this[i]){
        flag = false
      }
    }
    return flag
  }
}

function Student(fname,lname){
  this.fname = fname,
  this.lname = lname,
  this.courses = []
}

Student.prototype.name = function(){
  return this.fname + ' ' + this.lname;
};

Student.prototype.enroll = function(course){
  if (this.courses.indexOf(course) == -1){
    if (!this.hasConflict(course)){
      this.courses.push(course);
      //course.students.push(this);
      course.addStudent(this);
    }
  } else {
    console.log("Conflict!!!");
  }
};

Student.prototype.hasConflict = function(course){
  var flag = false;
  for(var i = 0; i < this.courses.length; i++){
    if (this.courses[i].conflictsWith(course)){
      flag = true;
    }
  }
  return flag;
};

Student.prototype.courseLoad = function(){
  var load = new Object();
  for (var i = 0; i < this.courses.length; i++){
    if (load[this.courses[i].department]) {
      load[this.courses[i].department] += this.courses[i].credits;
    } else {
      load[this.courses[i].department] = this.courses[i].credits;
    }
  }
  return load
};

function Course(name, department, credits, days, block){
  this.name = name,
  this.department = department,
  this.students = [],
  this.credits = credits,
  this.days = days,
  this.block = block
}

Course.prototype.addStudent = function(student){
  if (this.students.indexOf(student) == -1){
    this.students.push(student);
    //student.courses.push(this);
    student.enroll(this);
  }
};

Course.prototype.conflictsWith = function(course){
  var uniqDays = this.days.concat(course.days).my_uniq();
  console.log(typeof uniqDays)
  console.log(typeof [1,2,3])
  if (this.block != course.block){
    return false;
  } else if(uniqDays.compare(this.days.concat(course.days))) {
    return false;
  } else {
    return true;
  }
};


var s1 = new Student("Tommy", "Duek")
var c1 = new Course("Javascript", "App Academy", 5,["mon","wed","fri"],1)
var c2 = new Course("Rails", "App Academy", 10,["tues","thurs", "fri"],4)
s1.enroll(c1)
s1.enroll(c2)

console.log(s1.courses)









