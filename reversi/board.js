var piece = require('./piece.js');

exports.myBoard = (function(){
var Board = function Board(){
  this.grid = new Array(8)
  for (var i = 0; i < 8; i++){
    this.grid[i] = new Array(8);
  }
  this.grid.pos([3,3], new piece.myPieces.Piece("white", [3,3]));
  this.grid.pos([3,4], new piece.myPieces.Piece("black", [3,4]));
  this.grid.pos([4,4], new piece.myPieces.Piece("white", [4,4]));
  this.grid.pos([4,3], new piece.myPieces.Piece("black", [4,3]));

};

Array.prototype.pos = function(pos, obj){
  return this[pos[0]][pos[1]] = obj;
};

Array.prototype.atPos = function(pos){
  return this[pos[0]][pos[1]];
};

Array.prototype.plus = function(arr){
  var finalArr = [this[0] + arr[0], this[1] + arr[1]];
  return finalArr
};

Array.prototype.onBoard = function(){
 return (this[0] >= 0) && (this[1] >= 0) && (this[0] <= 7) && (this[1] <= 7)
}

Board.prototype.render = function(){
  var rendered = new Array(8)
  for (var x = 0; x < 8; x++){
    rendered[x] = new Array(8);
  }

  for(var i = 0; i < this.grid.length; i++){
    for(var j = 0; j < this.grid.length; j++){
      var obj = this.grid.atPos([i,j])
      if (obj){
        if (obj.color == "white"){
          rendered[i][j] = "W";
        } else {
          rendered[i][j] = "B";
        }
      } else {
       rendered[i][j] = " ";
      }
    }
  }
  console.log(rendered);
};

Board.prototype.validMove = function(color, pos){
  var flag = false;
  var dirs = [[1,0],[0,1],[-1,0],[0,-1]];

  for(var i = 0; i < dirs.length; i++){
    var nextPos = pos.plus(dirs[i]);
    if (nextPos.onBoard()){
      var nextSq = this.grid.atPos(nextPos);
    } else {
      nextSq = false;
    };

    //var nextSq = this.grid.atPos(pos.plus(dirs[i]))
    if (nextSq && nextSq.color != color){
      while (nextSq && nextSq.pos.onBoard){
        nextSq = this.grid.atPos(nextSq.pos.plus(dirs[i]))
        if (nextSq && nextSq.color == color){
          flag = true;
          break;
        }
      }
    }
  }
  return flag
};

Board.prototype.validMoves = function(color){
  var valids = [];
  for (var i = 0; i< this.grid.length; i++){
    for (var j=0; j< this.grid.length; j++){
      if (this.validMove(color,[i,j])){
        valids.push([i,j])
      }
    }
  }
  return valids
};

  return {Board: Board, Piece: piece.myPieces.Piece};

})();










