//var myPieces;
exports.myPieces = (function(){
  var Piece = function Piece(color, pos){
    this.color = color,
    this.pos = pos
  };

  var reverse = Piece.prototype.reverse = function() {
    this.color = (this.color == "white") ? "black" : "white";
  };

  return {Piece: Piece};
})();












// var p = new Piece("white", [1,5]);
// console.log(p.color);
// p.reverse();
// console.log(p.color);