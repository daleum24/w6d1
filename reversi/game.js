var board = require('./board.js');

function Game(){
  this.board = new board.myBoard.Board()
}
// validMove, pos, onBoad, atPos, plus, validMoves

Game.prototype.placePiece = function(color, pos){
  if (this.board.validMove(color, pos)){
    var p = new board.myBoard.Piece(color, pos);
    this.board.grid.pos(pos, p);
    this.flipMiddlePieces(pos, color);
  } else {
    return false
  }
};

Game.prototype.flipMiddlePieces = function(pos, color){
  var dirs = [[1,0],[0,1],[-1,0],[0,-1]];

  for(var i = 0; i < dirs.length; i++){
    var collector = [];

    var nextPos = pos.plus(dirs[i]);
    if (nextPos.onBoard()){
      var nextSq = this.board.grid.atPos(nextPos);
    } else {
      nextSq = false;
    };

    if (nextSq && nextSq.color != color){
      collector.push(nextSq)
      // console.log(nextSq)
      while (nextSq && nextSq.pos.onBoard){
        nextPos = nextSq.pos.plus(dirs[i]);
            if (nextPos.onBoard()){
              nextSq = this.board.grid.atPos(nextPos);
            } else {
              nextSq = false;
            };

        //nextSq = this.board.grid.atPos(nextSq.pos.plus(dirs[i]))
        //console.log(nextSq)
        collector.push(nextSq)
        if (nextSq && nextSq.color == color){
          for(var j = 0; j < collector.length; j++){
            this.board.grid[collector[j].pos[0]][collector[j].pos[1]] = new board.myBoard.Piece(color,collector[j].pos);
          }
          break;
        }
      }
    }
  }
};


var g = new Game();
g.placePiece("black", [2,3]);
g.placePiece("white", [2,4]);
g.placePiece("black", [2,5]);
//g.board.grid.atPos([2,5]).reverse()
g.board.render();
console.log(g.board.validMoves("white"));

// g.placePiece("white", [4,2]);

// console.log(g.board.grid.atPos([4,3]));


